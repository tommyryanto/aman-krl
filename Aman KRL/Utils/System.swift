
import UIKit

struct System {
    static func clearNavigationBar(forBar navBar: UINavigationBar?) {
        guard let navBar = navBar else {return}
        
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        navBar.backgroundColor = .clear
    }
}
