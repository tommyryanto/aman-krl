
import Foundation

extension String {
    static func greetings() -> String {
        let hour = Calendar.current.component(.hour, from: Date())
        
        switch hour {
        case 6..<12 : return "Good Morning"
        case 12..<17 : return "Good Afternoon"
        case 17..<24 : return "Good Evening"
        default: return "Undefined"
        }
    }
    
    //Be kind to each other today, Life is simple, We believe in keeping you save
    
    static func greetingsDescription() -> String{
        let hour = Calendar.current.component(.hour, from: Date())
        
        switch hour {
        case 6..<12 : return "Be kind to each other today"
        case 12..<17 : return "Life is simple"
        case 17..<24 : return "We believe in keeping you save"
        default: return "Undefined"
        }
    }
}
