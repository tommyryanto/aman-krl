
import Foundation
enum EmergencyCallType: Int {
    case fireFighter = 0
    case ambulance = 1
    case police = 2
    
    static let mapper: [EmergencyCallType : String] = [
        .fireFighter: "Pemadam Kebakaran",
        .ambulance: "Ambulans",
        .police: "Polisi"
    ]
    
    var divisionString: String {
        return EmergencyCallType.mapper[self] ?? "Empty"
    }
    
    static let phoneCall: [EmergencyCallType: String] = [
        .fireFighter: "113",
        .ambulance: "118",
        .police: "110"
    ]
    
    var phoneString: String{
        return EmergencyCallType.phoneCall[self] ?? "Empty"
    }
    
}

enum TipsType: Int {
    case tindakKejahatan = 0
    case halDilarang = 1
    case prosedurKeselamatan = 2
    case alatPemadamKebakaran = 3
    case paluPemecah = 4
    
    static let mapper: [TipsType : String] = [
        .tindakKejahatan: "Tindak Kejahatan",
        .halDilarang: "Hal-Hal yang Dilarang",
        .prosedurKeselamatan: "Prosedur Keselamatan",
        .alatPemadamKebakaran: "Alat Pemadam Kebakaran",
        .paluPemecah: "Palu Pemecah Kaca",
    ]
    
    static let shortenString: [TipsType: String] = [
        .tindakKejahatan: "Tindak Kejahatan",
        .halDilarang: "Hal yang Dilarang",
        .prosedurKeselamatan: "Prosedur Keselamatan",
        .alatPemadamKebakaran: "Alat Pemadam",
        .paluPemecah: "Palu Pemecah Kaca",
    ]
    
    var typeString: String {
        return TipsType.mapper[self] ?? "Empty"
    }
    
    var shortenString: String {
        return TipsType.shortenString[self] ?? "Empty"
    }
}

enum TipsCategoryTitle: Int{
    case caraNaik = 0
    case jagaBarang = 1
    case naikKRLKerja = 2
    case lainnya = 3
    
    static let mapper: [TipsCategoryTitle: String] = [
        .caraNaik: "Cara Naik KRL Bagi Pemula",
        .jagaBarang: "Jaga Barang Anda Saat Naik KRL",
        .naikKRLKerja: "Naik KRL Pada Saat Jam Kerja / Pulang Kerja",
        .lainnya: "Agar Dapat Duduk Di KRL Bagi Kaum Hawa"
    ]
    
    static let shortenString: [TipsCategoryTitle: String] = [
        .caraNaik: "Cara Naik KRL",
        .jagaBarang: "Jaga Barang Anda",
        .naikKRLKerja: "Jam Kerja",
        .lainnya: "Agar Dapat Duduk"
    ]
    
    static let image: [TipsCategoryTitle: String] = [
        .caraNaik: "naikKRL",
        .jagaBarang: "jagaBarang",
        .naikKRLKerja: "jamKerja",
        .lainnya: "lainnya"
    ]
    
    var tipsCategoryTitle: String {
        return TipsCategoryTitle.mapper[self] ?? "Empty"
    }
    
    var shortenString: String {
        return TipsCategoryTitle.shortenString[self] ?? "Empty"
    }
    
    var imageString: String {
        return TipsCategoryTitle.image[self] ?? "Empty"
    }
}
