
import Foundation

struct Tips {
    let name: String
    let description: String
    var isOpened: Bool
    
    static func generateTipsNaikKRL() -> [Tips] {
        return [
            Tips(name: "Ketahui Rute KRL", description: "Hal yang pertama harus dipersiapkan adalah mengetahui peta dan rute KRL. PT. Kereta Commuter Indonesia sudah menyiapkan peta rute KRL yang rapi dan mudah dibaca. Setelah itu, tentukanlah stasiun awal dan stasiun tujuan anda.", isOpened: false),
            Tips(name: "Siapkan Tiket KRL", description: """
Persiapkan tiket KRL anda sebelum menaiki KRL. Ada beberapa jenis tiket yang bisa digunakan untuk menaiki KRL, yaitu:

- Tiket Harian Berjaminan
Tiket Harian Berjaminan (THB) adalah tiket KRL elektronik yang perlu anda beli jika ingin melakukan satu kali perjalanan di hari yang sama. Pembelian THB dapat dilakukan di vending machine stasiun keberangkatan dengan harga Rp 10.000

- Kartu Multi Trip
Kartu Multi Trip alias KMT adalah tiket KRL elektronik yang bisa digunakan berulang-ulang dengan waktu yang tidak terbatas, selama saldo kartu masih ada. Saldo minimal dari KMT adalah 5.000. Harga awal kartu ini adalah 50.000 dengan saldo 30.000. Kalau saldo habis, bisa diisi ulang sendiri di mesin pengisian saldo KMT yang tersedia di stasiun.

- Kartu Bank dan e-money
Buat kamu para pengguna kartu bank atau e-money prabayar, kamu bisa menggunakan kartu seperti Mandiri E-Money, Brizzi, BNI TapCash, dan BCA Flazz.
""", isOpened: false),
            Tips(name: "Tap Tiket Elektronik Untuk Masuk Peron", description: "Setelah tiket KRL digenggam di tangan, saatnya masuk ke peron. Jangan lupa untuk tap alias menempelkan tiketmu di pintu masuk peron sampai terbuka.", isOpened: false),
            Tips(name: "Perhatikan Gerbong Khusus Wanita dan Anak - Anak", description: "Ini juga penting banget masuk ke dalam cara naik KRL yang harus kamu tahu. Rangkaian kereta terdiri dari beberapa gerbong, dengan gerbong pertama dan terakhir dikhususkan untuk wanita dan anak-anak. Jadi buat kamu yang cowok, jangan naik di gerbong pertama dan terakhir ya karena kamu pasti bakal ditegur sama petugas. Pemisahan gerbong ini dimasudkan untuk menghindari kemungkinan terjadinya hal-hal yang nggak diinginkan, seperti pelecehan seksual dan lainnya.", isOpened: false),
            Tips(name: "Perhatikan Orang yang Berhak Untuk Kursi Prioritas", description: "Di gerbong kereta tersedia kursi prioritas yang disediakan untuk ibu hamil, ibu yang membawa bayi, anak-anak, lansia, dan penumpang dengan disabilitas. Jadi, untuk penumpang lain yang nggak mau kriteria tersebut, jangan lupa untuk mendahulukan mereka yang lebih berhak untuk duduk di kursi prioritas. Jadi ingat poin cara naik KRL yang kelima ini demi kenyamanan bersama.", isOpened: false),
            Tips(name: "Tap Tiket Elektronik Stasiun Tujuan", description: "Perjalanan dengan KRL selesai! Sekarang saatnya buat tap lagi tiketmu ke perangkat pintu keluar. Jangan lupa kembalikan tiket buat yang memegang THB di loket terdekat. Selesai sudah!", isOpened: false),
        ]
    }
    
    static func generateTipsAmanNaikKRL() -> [Tips] {
        return [
            Tips(name: "Hati - hati Saat Menaiki Kereta", description: "Pada saat ingin menaiki kereta jangan berebut atau berdesakan ketika ramai karena pada saat itulah para kriminal mudah untuk beraksi, tunggulah kereta selanjutnya yang lebih senggang.", isOpened: false),
        Tips(name: "Taruh Barang di Tempat yang Aman", description: "Taruh barang anda ditempat yang aman dan mudah untuk diawasi oleh anda, digenggam adalah pilihan terbaik agar mudah diawasi, jangan menaruh barang berharga di saku celana, sebisa mungkin jangan menaruh barang anda pada tempat khusus barang diatas tempat duduk jika keadaan ramai, jika berat dan memang harus ditaruh, awasi selalu barang anda.", isOpened: false),
        Tips(name: "Awasi Barang Anda", description: "Awasi selalu barang anda sebelum maupun saat naik kereta, jika membawa tas taruh pada posisi depan anda agar mudah diawasi, jika menaruh barang pada tempat khusus barang, berdiri / duduklah di dekat barang anda.", isOpened: false)
        ]
    }
    
    static func generateTipsKesibukanKRL() -> [Tips] {
        return [
            Tips(name: "Bawa Gadget, Buku, dan Koran", description: "Terkadang perjalanan commuter line tidak semulus yang diperkirakan, bisa tertahan lama dan membosankan. Makanya butuh gadget, untuk mendengarkan musik, bisa buat update status, bisa chat, atau bisa membuat artikel. Buku atau koran juga bisa membantu supaya perjalanan dalam kereta tidak membosankan.", isOpened: false),
            Tips(name: "Masker", description: "Ini berfungsi untuk menutup wajah kalau ketiduran, bisa juga untuk menahan bau badan orang di kanan kiri atau berfungsi untuk menutup hidup jika sedang flu.", isOpened: false),
            Tips(name: "Bawa Obat dan Air Putih", description: "Bagi pengidap asma sepertinya harus selalu membawa obat, karena asupan oksigen sangat sedikit ketika keadaan penuh dan sesak. Untuk ibu-ibu hamil bawa minyak kayu putih atau sejenisnya jangan sampai muntah di dalam gerbong. Air putih sangat berguna saat dehidrasi melanda.", isOpened: false),
            Tips(name: "Gerak Cepat", description: "Layaknya seorang ninja berilmu tinggi kita harus bisa berlari secepat mungkin dan bergerak seperti kilat. Saat jam kerja kadang-kadang masinis seperti supir angkot yang lupa prosedur menutup pintu dengan baik, seharusnya pintu itu ditutup setelah petugas yang berdiri diperon memberi aba-aba dengan lambaian tangan, atau tiupan peluit. Tapi beberapa kali terjadi, tanpa aba-aba pintu sudah tertutup.", isOpened: false),
            Tips(name: "Mendahulukan Penumpang Turun", description: "Meski lagi terburu-buru karena takut telat berangkat kerja tapi secara etika dahulukan dulu penumpang yang turun, kan tidak lucu jika kita berselisih di pintu disebabkan bisa keluar masuk.", isOpened: false),
            Tips(name: "Memberikan Tempat Duduk Kepada Penumpang Prioritas", description: "Meskipun ada bangku “prioritas” untuk ibu hamil, lansia, Diffabel (penyandang cacat) dan Ibu membawa anak kecil. Yang kursinya tersedia dipojokan gerbong tetapi seharusnya kita berikan tempat duduk kita saat mereka berdiri persis depan muka.", isOpened: false),
            Tips(name: "Mengetahui Tujuan dan Rute KRL", description: "Ini juga tidak kalah penting, harus hafal atau mengetahui betul rute KRL yang akan kita tumpangi dan kita tuju. Pastikan agar stasiun keberangkatan dan stasiun tujuan sesuai dengan tempat kerja kita.", isOpened: false)
        ]
    }
    
    static func generateTipsKRLLainnya() -> [Tips] {
        return [
            Tips(name: "Tunggu Kereta yang Tepat", description: "Tunggu kereta yg feeder atau tunggu kereta berikutnya yg tidak begitu penuh lalu posisikan diri anda nunggu ditepi peron dekat pembatas warna (cari dekat pintu gerbong kereta yg datang) agar saat kereta sampai, anda bisa langsung masuk dan mencari tempat duduk yang kosong.", isOpened: false),
            Tips(name: "Cari Tempat yang Tepat", description: "Saat masuk kedalam gerbong kereta dan tidak mendapatkan tempat duduk, beridirilah didepan org org yg duduk tetapi tidak tidur. jangan bersandar dipintu kereta karena nanti susah mendapatkan tempat duduk ketika ada yg kosong (keburu didudukin org lain).", isOpened: false),
            Tips(name: "Teliti", description: "Pasang mata dan telinga anda! perhatikan bangku yang kosong setiap kereta berhenti distasiun. Dan pasang telinga anda jika ada yang mempersilahkan anda untuk duduk.", isOpened: false)
        ]
    }
}
