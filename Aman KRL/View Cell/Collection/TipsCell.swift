
import UIKit

class TipsCell: UICollectionViewCell {

    @IBOutlet weak var tipsImage: UIImageView!
    @IBOutlet weak var namaLabel: UILabel!
    @IBOutlet weak var deskripsiLabel: UILabel!
    @IBOutlet weak var view: UIView!
    
    let categoryDetail = ["How to report, when it happens.", "Get to know better about them.", "How to use it.", "How to use it.", "How to use it."]
    
    var rawValue: Int! {
        didSet {
            let name = TipsType(rawValue: rawValue)?.typeString
            let desc = self.categoryDetail[rawValue]
            
            namaLabel.text = name
            deskripsiLabel.text = desc
            tipsImage.image = UIImage(named: name ?? TipsType(rawValue: 0)!.typeString)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
