
import UIKit

protocol HeaderCellDelegate: class {
    func toggleCell(for indexPath: IndexPath?)
}

class TipsHeaderCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var view: UIView!
    
    weak var headerCellDelegate: HeaderCellDelegate?
    var indexPath: IndexPath?
    
    var type: Tips! {
        didSet {
            label.text = type.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let toggle = UITapGestureRecognizer(target: self, action: #selector(toggleCell))
        self.view.addGestureRecognizer(toggle)
    }
    
    @objc func toggleCell() {
        headerCellDelegate?.toggleCell(for: indexPath ?? IndexPath(row: 0, section: 0))
    }
    
}
