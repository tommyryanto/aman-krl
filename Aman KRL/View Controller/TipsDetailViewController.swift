
import UIKit

class TipsDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tipe: TipsCategoryTitle?
    var index: Int?
    var types = [Tips]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = TipsCategoryTitle(rawValue: index ?? 0)?.shortenString ?? "Naik KRL"
        if let tipe = tipe {
            switch tipe {
            case .caraNaik:
                types = Tips.generateTipsNaikKRL()
            case .jagaBarang:
                types = Tips.generateTipsAmanNaikKRL()
            case .naikKRLKerja:
                types = Tips.generateTipsKesibukanKRL()
            case .lainnya:
                types = Tips.generateTipsKRLLainnya()
            }
        }
        
        setupTableView()
    }
    
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let navbar = navigationController?.navigationBar else {return}
        
        navbar.setBackgroundImage(UIImage(named: TipsCategoryTitle(rawValue: index ?? 0)!.shortenString), for: .default)
        navbar.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80)
        navbar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
}

extension TipsDetailViewController: UITableViewDataSource, UITableViewDelegate, HeaderCellDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = Bundle.main.loadNibNamed("TipsHeaderCell", owner: self, options: nil)?.first as? TipsHeaderCell else {return UIView()}
        
        header.label.text = types[section].name
        header.iconImage.image = UIImage(named: "\(TipsCategoryTitle(rawValue: index ?? 0)?.imageString ?? "naikKRL")-step\(section + 1)")
        header.headerCellDelegate = self
        header.indexPath = IndexPath(row: 0, section: section)
        
        return header
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let _ = tipe else { return 0 }
        return types.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _ = tipe else { return 0 }
        return self.types[section].isOpened ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = types[indexPath.section].description
        cell.textLabel?.textAlignment = .justified
        cell.textLabel?.numberOfLines = 20
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func toggleCell(for indexPath: IndexPath?) {
        guard let indexPath = indexPath else { return }
        self.types[indexPath.section].isOpened = !self.types[indexPath.section].isOpened
        tableView.reloadData()
    }
    
}

