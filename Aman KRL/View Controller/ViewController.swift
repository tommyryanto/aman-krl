import UIKit

class ViewController: UIViewController {
    /* SOME VARIABLES */
    var backgroundColor: [UIColor] = [.blue, .brown, .cyan, .darkGray, .gray, .green, .magenta, .orange, .purple, .red, .yellow]
    let categoryDetail = ["How to report, when it happens.", "Get to know better about them.", "How to use it."]

    /* SOME OUTLETS */
    @IBOutlet weak var quotes: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var greetings: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var greetingsDescription: UILabel!
    
    /* VIEW CONTROLLER LIFE CYCLE */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupControllerView()
    }
    
    private func setupControllerView() {
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.numberOfPages = TipsType.mapper.count
        self.pageControl.hidesForSinglePage = true
        self.collectionView.isPagingEnabled = true
        self.collectionView.register(UINib(nibName: "TipsCell", bundle: nil), forCellWithReuseIdentifier: "TipsCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        greetings.text = .greetings()
        greetingsDescription.text = .greetingsDescription()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.collectionView.showsHorizontalScrollIndicator = false
    }
    
    /*PASSING DATA TO ANOTHER VC*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"{
            let destination = segue.destination as! DetailViewController
            let indexPaths = collectionView.indexPathsForSelectedItems
            let indexPath = indexPaths![0]
            let backItem = UIBarButtonItem()
            backItem.title = "Home"
            navigationItem.backBarButtonItem = backItem
            destination.titleType = TipsType(rawValue: indexPath.row)?.shortenString
            destination.desc = indexPath.row < 2 ? categoryDetail[(indexPath.row)] : categoryDetail[2]
            destination.image = UIImage(named: TipsType(rawValue: indexPath.row)!.typeString)
            destination.indexSelected = indexPath.row
        }
    }
    
}

//MARK:- Collection View Extension
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //TODO:- Set collection view items
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TipsType.mapper.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: self.view.frame.width / 8, bottom: 0, right: 0);
    }
    
    //TODO:- Set collection view cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TipsCell", for: indexPath as IndexPath) as! TipsCell

        cell.rawValue = indexPath.row
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = cell.frame.width / 12
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row - 1
    }
    
    //TODO:- Set collection view action when selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: self)
    }
    
}
