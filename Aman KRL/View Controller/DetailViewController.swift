
import UIKit

class DetailViewController: UIViewController {
    
    var titleType: String?
    var desc: String?
    var image: UIImage?
    @IBOutlet weak var imageHeader: UIImageView!
    var indexSelected: Int?
    
    //MAAFKAN GW MI, UDAH PUSING MESTI PAKE CARA APA LAGI :(
    
    var step: [String] = []
    var stepDetail: [String] = []
    var imageStep: [UIImage] = []
    
    func image( _ image:UIImage, withSize newSize:CGSize) -> UIImage {
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.automatic)
    }
    
    /* CONTENT FUNCTION */
    func tindakKejahatan(){
        step = ["Datangi kantor bagian keamanan di stasiun", "Dapatkan surat keterangan tindak pidana", "Kunjungi Kantor", "Datangi Kantor Terdekat", "Datang ke SPKT"]
    }
    
    func halDilarang(){
        step = ["Dilarang Duduk di Lantai & Menggunakan Kursi Lipat", "Dilarang Membuang Sampah Sembarangan", "Dilarang berjualan", "Dilarang membawa binatang", "Dilarang membawa benda berbau menyengat", "Dilarang membawa benda mudah terbakar", "Dilarang membawa senjata Api/Tajam Tanpa Izin", "Dilarang Makan & Minum", "Dilarang Merokok", "Dilarang Ngamen", "Dilarang melakukan kegiatan yang menimbulkan keributan", "Dilarang mencuri/Mengambil/Merusak Aset KRL & Stasiun", "Dilarang Mengemis dan Meminta sumbangan dalam bentuk apapun", "Dilarang melakukan perbuatan yang melanggar norma kesusilaan", "Dilarang berada di sambungan KRL", "Dilarang Menggunakan Handgrip KRL untuk bermain", "Dilarang membuka jendela, pintu, menarik tuas darurat kecuali dalam keadaan darurat", "Dilarang Masuk ke Kabin Masinis KRL tanpa izin tertulis", "Dilarang berada di kereta wanita bagi pengguna jasa berjenis kelamin Laki-laki, Kec. Anak-anak", "Dilarang melakukan kegiatan yang mengandung unsur politik & SARA"]
        
        for i in 1...23{
            if i == 18{
                imageStep.append(UIImage())
            }else{
                imageStep.append(image(UIImage(named: "halDiLarang\(i)")!, withSize: CGSize(width: 30, height: 30)))
            }
        }
    }
    
    func prosedurKeselamatan(){
        step = ["Jangan panik, cari tombol merah/SOS dan tekan tombol tersebut. Terdapat di bar bagian kanan bawah atau saat di dalam kereta Biasanya ada 2 tombol didalam 1 kereta, tergantung jenis KRLnya.", "Tunggu dan dengarkan arahan atau instruksi dari petugas di kabin masinis.", "Jika tidak mendengar arah atau instruksi dari petugas terkait, segera cari keran pintu darurat yg ada di dekat pintu kereta.", "Biasanya keran darurat berada dibawah kursi penumpang, ada juga yg diatas pintu (untuk seri 205).", "Putar tuas/keran tersebut untuk membuka 1 pintu terdekat dari tuas/keran tersebut.", "Atau bisa melalui keran pintu darurat yg berada pada dinding kereta.. ini untuk membuka pintu 1 kereta, bukan 1 rangkaian.", "Setelah tuas/keran pintu darurat diputar, buka pintu secara manual menggunakan tangan.", "Turunlah dari rangkaian dengan keadaan tidak panik apalagi saling dorong mendorong, tetap utamakan faktor keselamatan sesama penumpang.", "Cara lain dengan cara menurunkan atau membuka jendela, lalu keluar melalui jendela jika keadaan benar2 mendesak."]
    }
    
    func alatPemadam(){
        step = ["Tarik Kunci Pengaman", "Pegang bagian ujung selang", "Arahkan selang ke sumber api", "Tekan luas bagian atas sepenuh", "Sapukan dari satu sisi ke sisi lainnya"]
    }
    
    func paluPemecah(){
        step = ["Hancurkan panel jendela dengan palu pemecah kaca.", "Dengan menggunakan tangan, tekan keluar sudut atas panel jendela"]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch indexSelected {
        case 0:
            tindakKejahatan()
        case 1:
            halDilarang()
        case 2:
            prosedurKeselamatan()
        case 3:
            alatPemadam()
        case 4:
            paluPemecah()
        default:
            print("error")
        }
        tableView.dataSource = self
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        System.clearNavigationBar(forBar: self.navigationController?.navigationBar)
        
        guard let navbar = self.navigationController?.navigationBar else {return}
        navbar.prefersLargeTitles = true
        title = titleType ?? "Empty"
        navbar.tintColor = UIColor.white
        navbar.setBackgroundImage(image, for: .default)
        navbar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navbar.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80)
        imageHeader.image = self.image
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return step.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        cell?.textLabel?.text = step[indexPath.row]
        cell?.textLabel?.numberOfLines = 0
        cell?.imageView?.image = imageStep.isEmpty ? nil : imageStep[indexPath.row]
        
        return cell!
    }
    
    
}
