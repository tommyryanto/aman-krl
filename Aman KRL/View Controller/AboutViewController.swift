
import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: "TipsCategoryCell", bundle: nil), forCellReuseIdentifier: "TipsCategoryCell")
    }

}

extension AboutViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EmergencyCallType.mapper.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TipsCategoryCell") as! TipsCategoryCell
        cell.imageBg.image = UIImage(named: EmergencyCallType(rawValue: indexPath.row)!.divisionString)
        cell.label.text = EmergencyCallType(rawValue: indexPath.row)?.divisionString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.callSOS(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    private func callSOS(_ row: Int) {
        let call = UIAlertController(title: EmergencyCallType(rawValue: row)?.divisionString, message: EmergencyCallType(rawValue: row)?.phoneString, preferredStyle: .alert)
        call.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
            if let url = URL(string: "tel://\(EmergencyCallType(rawValue: row)?.phoneString ?? "Empty")"){
                UIApplication.shared.canOpenURL(url)
            }
        }))
        call.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            call.dismiss(animated: true, completion: nil)
        }))
        self.present(call, animated: true, completion: nil)
    }
    
}

